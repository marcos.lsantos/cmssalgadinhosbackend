const config = require('config')
const MSGS = require('../messages')
const aws = require('aws-sdk')
const fs = require('fs')

function slugify(string){
    
}


module.exports = async(req, res, next)=>{
    try{
        const BUCKET_NAME = process.env.S3_BUCKET_NAME || config.get("S3_BUCKET_NAME")
        
        const s3 = new AWS.S3({
            accessKeyId: process.env.AWS_KEY_ID || config.get("AWS_KEY_ID"),
            secretAccessKey: process.env.AWS_SECRET_KEY || config.get("AWS_SECRET_KEY")
        })
        if(!req.files){
            res.status(204).send({error: MSGS.FILE_NOT_SENT})
        }else{
            let photo = req.files.photo
            const name = slugify(photo.name)
            req.body.photo_name = name

            if(photo.mimetype.includes('image/')){
                const file = await photo.mv(`./uploads/${name}`)

                const params ={ 
                    bucket: BUCKET_NAME,
                    ACL: 'public-read',
                    Key: `product/${name}`,
                    Body: fs.createReadStream(`./uploads/${name}`)
                }

                s3.upload(params,(err, data)=>{
                    if(err){
                        console.error(err);
                        res.status(500).send(err)
                    }else{
                        console.log(`File upload Successfuly. ${data.Location}`)
                        fs.unlinkSync(`./uploads/${name}`)
                        next()
                    }
                })
            }else{
                res.status(400).send({error: MSGS.FILE_INVALID_FORMAT})
            }
        }

    }catch(err){
        res.status(500).send({"error": err.message})

    }
}