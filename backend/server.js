const express = require ('express')
var bodyParser = require ('body-parser')
const connectDB = require ('./config/db')
const app = express()
var cors = require('cors')
const PORT = process.env.PORT || 3004

//Middleware
app.use(express.json())
app.use(bodyParser.urlencoded({extended: true}))
app.use(bodyParser.json())
app.use(cors())

//Connect Database
connectDB()

//Routes
app.use('/', require('./routes/api/init'))
app.use('/auth', require('./routes/api/auth'))
app.use('/user', require('./routes/api/user'))
app.use('/product', require('./routes/api/product'))

app.listen(PORT, () => {console.log(`port ${PORT}`)})