const  express = require('express');
const router = express.Router()
const User = require('../../models/user')
const bcrypt = require('bcryptjs')
const jwt = require('jsonwebtoken')
const config = require('config')
const {check, validationResult} = require ('express-validator');
const MSGS = require('../../messages')

router.post('/',[
        check('email', MSGS).isEmail(),
        check('password', MSGS).exists()
    ], async(req, res) =>{
        const errors = validationResult(req);
        if(!errors.isEmpty()){
            return res.status(400).json({errors: errors.array()})
        }
        const {email, password} = req.body

        try{
            let user = await User.findOne({email}).select('id password email name')
            if(!user){
                return res.status(404).json({errors:[{msg: MSGS}]})
            }else{
                const isMatch = await bcrypt.compare(password, user.password)
                if(!isMatch){
                    return res.status(404).json({errors:[{msg: MSGS}]})
                }else{
                    const payload = {
                        user:{
                            id: user.id,
                            name: user.name
                        }
                    }
                    jwt.sign(payload, config.get('jwtSecret'),{expiresIn: '5 days'},
                    (err, token) =>{
                        if(err) throw err
                        payload.token = token
                        res.json(payload)
                        }
                    )
                }
            }
        }catch(err){
            console.error(err.message)
            res.status(500).send('Server error')
        }
})

module.exports = router;