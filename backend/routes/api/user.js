const { Router } = require('express')
const express = require('express')
const router = express.Router()
const User = require('../../models/user')
const auth = require('../../midleware/auth')
const MSGS = require('../../messages')
const bcrypt = require('bcryptjs')
const { check, validationResult } = require('express-validator')


//@route /user
//@desc list users
//@access aunthenticate
router.get('/', auth, async(req, res, next) =>{
    try{
        const user = await User.find({})
        res.json(user)
    }catch(err){
        console.error(err)
        res.status(500).send({"error": MSGS.GENERIC_ERROR})
    }

})


//@route /user
//@desc save user
//@access aunthenticate

router.post('/',auth, [
    check('email', 'email is not valid').isEmail(),
    check('name').not().isEmpty(),
    check('password', 'Please enter a password with 5 or more characters').isLength({min: 5})
], async(req, res, next) =>{
    try{ 
        let {name, email, password }= req.body
        const errors = validationResult(req)
    
        if (!errors.isEmpty()) {
            return res.status(400).json({ errors: errors.array() })
          } else {
                let usuario =  new User({name, email, password})
                const salt = await bcrypt.genSalt(10)
                usuario.password = await bcrypt.hash(password, salt)

                await usuario.save()
                if(usuario.id){
                    res.json(usuario)
                }
            }
    }catch(err){
        console.error(err.message)
        res.status(500).send({ "error1": MSGS.GENERIC_ERROR })      
    }
})


//@route /user/:id
//@desc  edit user
//@access aunthenticate
router.patch('/:id', auth, async(req, res, next) => {
    try {
        const errors = validationResult(req)

        if(!errors.isEmpty()){
            return res.status(404).json({errors: errors.array()})
        }
        
        const id = req.params.id
        const salt = await bcrypt.gensalt(10)
        let bodyRequest = req.body

        if(bodyRequest.password){
            bodyRequest.password = await bcrypt.hash(bodyRequest.password, salt)
        }
        const update = {$set: bodyRequest}
        const user = await User.findOneAndUpdate(id, update, {new: true})

        if(user){
            res.json(user)
        }else{
            res.status(404).send({error: MSGS.USER404})
        }
    }catch(err){
        console.error(err.message)
        res.status(500).send({"error": MSGS.GENERIC_ERROR})
    }
})



//@route /user/:id
//@desc delet user
//@access aunthenticate
router.delete('/:id', auth, async(req, res, next) => {
    try{
        const id = req.params.id

        const user = await User.findOneAndDelete({_id: id})

        if(user){
            res.json(user)
        }else{
            res.status(404).send({error: MSGS.USER404})
        }
    }catch(err){
        res.json(err)
        res.status(500).send({"error": MSGS.GENERIC_ERROR})
    }
})

module.exports = router