
const MSGS = {
    'CATEGORY404' : 'Categoria não encontrada',
    'CONTENT404' : 'Conteúdo não encontrada',
    'GENERIC_ERROR': 'Erro!',
    'INVALID_TOKEN' : 'Token Inválido',
    'PASSWORD_INVALID' : 'Senha incorreta!',
    'PRODUCT404' : 'Produto não encontrado',
    'REQUIRED_PASSWORD' : 'Por favor, insira sua senha.', 
    'USER404' : 'Usuário não encontrado',
    'VALID_EMAIL' : 'Por favor, insira um email válido.',
    'WITHOUT_TOKEN' : 'Token não enviado',
}

module.exports = MSGS