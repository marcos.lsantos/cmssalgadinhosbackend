const moongose = require('mongoose')

const categorySchema = new moongose.Schema({
    name:{
        type: String,
        required: true
    },
    icon:{
        type: String,
        required: true
    }

}, {autoCreate: true})

module.exports = moongose.model('category', categorySchema)