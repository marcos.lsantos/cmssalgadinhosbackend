const moongose = require('mongoose')

const productSchema = new moongose.Schema({
    photo: {
        type: String,
        required: true
    },
    title:{
        type: String,
        reuired: true
    },
    category:{
        type: moongose.Schema.Types.ObjectId,
        ref: 'category',
        required: true
    },
    highlight:{
        type: Boolean,
        default: false
    },
    description:{
        type: String,
        required: true
    },
    price: {
        type: Number,
        min: 1,
        required: true
    },
    discount_price:{
        type: Number,
        min: 1
    },
    discount_price_percent: {
        type: Number,
        min: 1
    },
    last_modify_by:{
        type: moongose.Schema.ObjectId,
        ref: 'user'
    },
    last_modification_date: {
        type: Date,
        default: Date.now
    },
    status:{
        type: Boolean,
        default: true
    }
}, {autoCreate: true})

module.exports = moongose.model ('product', productSchema)